Avant le lancement du projet, il faut être en possession de tous les fichiers rendus (images, json, db, exe, etc...). L'application ne peut être fonctionnelle sans. 
De plus, certaines bibliothèques sont à télécharger :
-customtkinter;
-pillow;
-smtplib;
-pandas;
- winotify;
-json.

Concernant son lancement, il est impératif que tous les fichiers soient dans un même gros dossier, avec des sous dossiers. 
Concernant, les polices, il faudra les télécharger grâce aux fichiers zip mis à disposition.

Enfin, les applications Chrome, Youtube et Gmail doivent se situer sur le Bureau sinon leur chemin d'accès ne sera pas valide. 

Il serait préférable de lire l'application avec Visual Studio Code, d'autant plus que le dossier pourrait s'avérer très lourd.